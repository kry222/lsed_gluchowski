rm(list=ls());
library(MASS)
library(e1071)

# Generowanie
draw.data.gauss <- function(S1, S2, m1, m2, n1, n2) {
  
  X1 <- mvrnorm(n1, m1, S1)
  X2 <- mvrnorm(n2, m2, S2)
  
  X1 <- data.frame(X1); colnames(X1) <- c("x", "y")
  X2 <- data.frame(X2); colnames(X2) <- c("x", "y")
  
  X1$class <- 1; X2$class <- 2
  
  data <- rbind(X1, X2); data$class <- factor(data$class)
  
  return(data)
}

# Rysowanie punktów
plot.data <- function(data) {
  
  cols <- c("blue", "red")
  
  plot(data[,1:2], col = cols[data$class], cex = 2.5, pch = 19)
  text(data[,1:2], labels = 1:nrow(data), cex = 0.8, col = "white", font = 2)
  
}


# Wyznaczanie blędu klasyfikatora
err.rate <- function(org.class, pred.class) {
  
  CM <- table(org.class, pred.class)
  
  return(1 - sum(diag(CM)) / sum(CM))
}

# Parametry danych z rozkładu Gaussa
S1 <- matrix(c(4, 2, 2, 4), 2, 2)
S2 <- matrix(c(4, 2, 2, 2), 2, 2)

m1 <- c(-1, -1)
m2 <- c(2, 2)

n1 <- 30
n2 <- 20

# Ustawienie ziarna dla losowania danych
set.seed(128)

# Generowanie obserwacji
data <- draw.data.gauss(S1, S2, m1, m2, n1, n2)


#Pętla dla C od 0,001 do 0.2
C<- c(1:200)/1000
C
err<-1
for (i in 1:1000) {
  data.svm <- svm(class ~ x + y, type = "C-classification", data = data, cost = C[i], scale = F, kernel = "linear")
  err[i]<-err.rate(predict(data.svm,data),data$class)
}


plot(C,err, xlab = "Wartość parametru C", ylab = "Współczynnik błędnych klasyfikacji", main = "Wykres zależności błednych klasyfikacji od parametru C")

#Klasyfikator LDA
data.lda <-lda(class ~ x + y, data = data)
err.rate(predict(data.lda,data)$class,data$class)  #Dokładność 0.14
#Sprawdzenie dla jakich C err.rate osiąga najmniejszą wartość czyli 0.12
C[which(err==0.12)]
  