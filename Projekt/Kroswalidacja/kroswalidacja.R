rm(list = ls())

library(MASS)
library(e1071)
library(class)
library(rpart)

setwd("/home/krystian/lsed_gluchowski/Projekt")
dane<-read.table("HTRU_2.csv", sep = ",", col.names = c("Mean of the integrated profile", "Standard deviation of the integrated profile", "Excess kurtosis of the integrated profile",
                                                        "Skewness of the integrated profile", "Mean of the DM-SNR curve", "Standard deviation of the DM-SNR curve", "Excess kurtosis of the DM-SNR curve", "Skewness of the DM-SNR curve","CLASS"))

dane$CLASS<-factor(dane$CLASS)
table(dane$CLASS)
ACCURACY <- function(data, predict){
  tb<-table(data,predict)
  AC <- c(ACCURACY =sum(diag(tb))/sum(tb), PURITY = tb[2,2]/sum(tb[,2]))
  return(AC)
}


CPopt <- function(tab){
  i.cp.min <- which.min(tab$xerror)
  line <- tab$xerror[i.cp.min] + tab$xstd[i.cp.min]
  i<- min(which(tab$xerror < line))
  return(tab$CP[i])
}

findoptk <- function(PU){
  PU1<-PU[1:(floor(nrow(PU)*0.1)),]
  PU2<-PU[(floor(nrow(PU)*0.1)+1):nrow(PU),]
  tmp<-floor(min(table(PU2$CLASS))/4)
  C<-sapply(1:tmp,function(i) ACCURACY(PU1$CLASS,knn(PU2, PU1, PU2$CLASS, i)))
  k<-which.max(C)
  return(k)
}



FISHER<- function(PU){
  PU0<- PU[which(PU$CLASS == 0),1:(ncol(PU)-1)]
  PU1<- PU[which(PU$CLASS == 1),1:(ncol(PU)-1)]
  
  mi1<-sapply(1:(ncol(PU)-1), function(i) mean(PU1[,i]))
  mi0<-sapply(1:(ncol(PU)-1), function(i) mean(PU0[,i]))
  
  S0<-cov(PU0)
  S1<-cov(PU1)
  
  W<-((nrow(PU0)-1)*S0+ (nrow(PU1)-1)*S1)/(nrow(PU)-2)
  a<- ginv(W) %*% (mi1-mi0)
  flist<- list(a = a, mi0 = mi0, mi1 =mi1)
  return(flist)
}

################KROSWALIDACJA
CV.fis <- function(data) {
  K<-5
  N <- nrow(data)       #########data
  data.rnd <- data[sample(1:N),]
  sets <- sapply(1:K, function(i) ((i-1) * floor(N/K) + 1):(i * floor(N/K)))
  if(is.vector(sets)) sets <- t(as.matrix(sets))
  res <- t(sapply(1:K, function(k) CV.main.fis(data.rnd[-c(sets[,k]),], data.rnd[sets[,k],])))
  mean(res)
}

CV.main.fis <- function(learn, test) {
  a<-FISHER(learn)
  test.pred<-sapply(1:nrow(test), function(i){
    if(abs(t(a$a)%*%as.numeric(test[i,1:(ncol(learn)-1)]-a$mi0))<abs(t(a$a)%*%as.numeric(test[i,1:(ncol(learn)-1)]-a$mi1))) return(0)
    else return(1)
  })
  CM <- table(test$CLASS, test.pred)
  1- (sum(diag(CM))/sum(CM))
} 


CV.lda <- function(data) {
  K<-5
  N <- nrow(data)       #########data
  data.rnd <- data[sample(1:N),]
  sets <- sapply(1:K, function(i) ((i-1) * floor(N/K) + 1):(i * floor(N/K)))
  if(is.vector(sets)) sets <- t(as.matrix(sets))
  res <- t(sapply(1:K, function(k) CV.main.lda(data.rnd[-c(sets[,k]),], data.rnd[sets[,k],])))
  mean(res)
}


CV.main.lda <- function(learn, test) {
  learn.classifier <- lda(CLASS~.,learn)
  test.pred <- predict(learn.classifier, newdata = test)
  CM <- table(test$CLASS, test.pred$class)
  1- (sum(diag(CM))/sum(CM))
} 

CV.qda <- function(data) {
  K<-5
  N <- nrow(data)       #########data
  data.rnd <- data[sample(1:N),]
  sets <- sapply(1:K, function(i) ((i-1) * floor(N/K) + 1):(i * floor(N/K)))
  if(is.vector(sets)) sets <- t(as.matrix(sets))
  res <- t(sapply(1:K, function(k) CV.main.qda(data.rnd[-c(sets[,k]),], data.rnd[sets[,k],])))
  mean(res)
}


CV.main.qda <- function(learn, test) {
  learn.classifier <- qda(CLASS~.,learn)
  test.pred <- predict(learn.classifier, newdata = test)
  CM <- table(test$CLASS, test.pred$class)
  1- (sum(diag(CM))/sum(CM))
} 

CV.bay <- function(data) {
  K<-5
  N <- nrow(data)       #########data
  data.rnd <- data[sample(1:N),]
  sets <- sapply(1:K, function(i) ((i-1) * floor(N/K) + 1):(i * floor(N/K)))
  if(is.vector(sets)) sets <- t(as.matrix(sets))
  res <- t(sapply(1:K, function(k) CV.main.bay(data.rnd[-c(sets[,k]),], data.rnd[sets[,k],])))
  mean(res)
}


CV.main.bay <- function(learn, test) {
  learn.classifier <- naiveBayes(CLASS~.,learn)
  test.pred <- predict(learn.classifier, newdata = test)
  CM <- table(test$CLASS, test.pred)
  1- (sum(diag(CM))/sum(CM))
} 


CV.drz <- function(data) {
  K<-5
  N <- nrow(data)       #########data
  data.rnd <- data[sample(1:N),]
  sets <- sapply(1:K, function(i) ((i-1) * floor(N/K) + 1):(i * floor(N/K)))
  if(is.vector(sets)) sets <- t(as.matrix(sets))
  res <- t(sapply(1:K, function(k) CV.main.drz(data.rnd[-c(sets[,k]),], data.rnd[sets[,k],])))
  mean(res)
}


CV.main.drz <- function(learn, test) {
  
  tree.clas<-rpart(CLASS~.,learn, maxdepth = 1, minbucket = 1, cp = 0)
  tree.clas<-prune(tree.clas, cp = CPopt(data.frame(tree.clas$cptable)))

  CM <- table(predict(tree.clas,test, type = "class"),test$CLASS)
  1- (sum(diag(CM))/sum(CM))
} 

CV.knn <- function(data, kopt) {
  K<-5
  N <- nrow(data)       #########data
  data.rnd <- data[sample(1:N),]
  sets <- sapply(1:K, function(i) ((i-1) * floor(N/K) + 1):(i * floor(N/K)))
  if(is.vector(sets)) sets <- t(as.matrix(sets))
  res <- t(sapply(1:K, function(k) CV.main.knn(data.rnd[-c(sets[,k]),], data.rnd[sets[,k],],kopt)))
  mean(res)
}


CV.main.knn <- function(learn, test,kopt) {
  
  CM <- table(test$CLASS, knn(learn,test,learn$CLASS, kopt))
  1- (sum(diag(CM))/sum(CM))
} 

##########################################KONIEC



WIELKA_FUNKCJA<- function(dane,kolumny){
  X<- dane[,kolumny]
  X <-X[sample(1:nrow(dane)),]
  podzial <- c(floor(0.7*nrow(dane)),  nrow(dane))
  PU<- X[1:podzial[1],]
  PT<-X[(podzial[1]+1):podzial[2],]
  
  #1 Fisher
  a<-FISHER(PU)
  accuracy.err<- CV.fis(PU)
  
  
  #2 LDA
  lda.clas<-lda(CLASS~.,PU)
  accuracy.err <- rbind(accuracy.err,CV.lda(PU))
  
  
  #3 QDA 
  qda.clas<-qda(CLASS~.,PU)
  accuracy.err <- rbind(accuracy.err, CV.qda(PU))
  
  #4 NAIVNY BAYES
  bayes.clas<-naiveBayes(CLASS~.,PU)
  accuracy.err<- rbind(accuracy.err,CV.bay(PU))
  
  #5 DRZEWO
  tree.clas<-rpart(CLASS~.,PU, maxdepth = 1, minbucket = 1, cp = 0)
  tree.clas<-prune(tree.clas, cp = CPopt(data.frame(tree.clas$cptable)))
  accuracy.err<- rbind(accuracy.err,CV.drz(PU))
  
  #6 KNN
  kopt<-findoptk(PU)
  #kopt<-7
  accuracy.err<- rbind(accuracy.err, CV.knn(PU,kopt))
  
  nazwy<-c("FISHER", "LDA", "QDA", "NAIWNY BAYES", "DRZEWO", "KNN")
  i<-which.min(accuracy.err)
  lista<- list(NAZWA.AC = nazwy[i])
  
  if(i==1) lista$ac.err<- 1-ACCURACY(PT$CLASS,sapply(1:nrow(PT), function(i){
    if(abs(t(a$a)%*%as.numeric(PT[i,1:(ncol(PT)-1)]-a$mi0))<abs(t(a$a)%*%as.numeric(PT[i,1:(ncol(PT)-1)]-a$mi1))) return(0)
    else return(1)
  }))
  else if(i==2) lista$ac.err <- 1-ACCURACY(predict(lda.clas,PT)$class,PT$CLASS)
  else if(i==3) lista$ac.err <- 1 - ACCURACY(predict(qda.clas,PT)$class,PT$CLASS)
  else if(i==4) lista$ac.err <- 1-ACCURACY(predict(bayes.clas,PT),PT$CLASS)
  else if(i==5) lista$ac.err <- 1-ACCURACY(predict(tree.clas,PT, type = "class"),PT$CLASS)
  else lista$ac.err <-  1-ACCURACY(knn(PU,PT,PU$CLASS, kopt),PT$CLASS)
  
  
  return(lista)
}


#kolumny <-c(1:9)       #"FullCol.100it.txt"
#kolumny<-c(1:4,9)         #"14Col.100it.txt"
#kolumny<- c(5:9)          #"58Col.100it.txt"
#kolumny<-c(1:3,9)          #"13Col.100it.txt"
#kolumny<-c(1:9)           #"57Col.100it.txt"
kolumny<-c(1:2,9)

tmp[2]<-list(WIELKA_FUNKCJA(dane,kolumny))
#{"FULL","1:2","1:3","1:4","124","1256","1278","3478","3456","5:7","5:8","5:6","568”}


for (i in 2:100) {
  tmp[i]<-list(WIELKA_FUNKCJA(dane,kolumny))
}
nazwaAC <- tmp[[1]]$NAZWA.AC
AC <- tmp[[1]]$ac.err
nazwaPU <- tmp[[1]]$NAZWA.PU
PU<- tmp[[1]]$pu.err
tabela <- data.frame(nazwaAC,t(AC),nazwaPU,t(PU))
for (i in 2:100){
  nazwaAC <- tmp[[i]]$NAZWA.AC
  AC <- tmp[[i]]$ac.err
  nazwaPU <- tmp[[i]]$NAZWA.PU
  PU<- tmp[[i]]$pu.err
  tabela1 <- data.frame(nazwaAC,t(AC),nazwaPU,t(PU))
  tabela<-rbind(tabela,tabela1)
}

write.table(tabela, file = "56Col.100it.txt", sep = ",")

