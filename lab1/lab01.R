rm(list = ls())
#wczytanie bibliotek
library(MASS)
library(klaR)
library(mvtnorm)
#wartości początkowe m i S dla dwóch rozkładów
S=matrix(c(4,0,0,4),2,2)
m1=c(-3,-1)
m2=c(2,2)
n1<-40
n2<-30
n<-n1+n2
#tworzenie grup uczących dla klasy 1 i 2
X1=mvrnorm(n1,m1,S)
X2=mvrnorm(n2,m2,S)
#utworzenie ramki z danych i nazwanie kolumn X, Y
X1<-data.frame(X1)
colnames(X1)<-c("X","Y")

X2<-data.frame(X2)
colnames(X2)<-c("X","Y")

#nadanie klas grupom
X1$class <-1
X2$class <-2

#a priori
pi1<-n1/n
pi2<-n2/n

#srednie
m11<-apply(X1, 2, mean)
m22<-apply(X2, 2, mean)

#macierze kowariancji
S11<-cov(X1)
S22<-cov(X2)

#przypisanie do jednej tabelki
data<-rbind(X1,X2)
#przemiana zmiennych z class na zmienne jakościowe
data$class<-factor(data$class)

#funkcja licząca p. posteriori wg dyskryminacji Bayesa dla całej powierzchni XY na podstawie wytycznych pi m S 
Bayes_posteriori_map<-function(mapa,m11,m22,S11,S22,pi1,pi2){
  X1<- pi1*dnorm(mapa,m11["X"],sqrt(S11['X','X']))%*%t(dnorm(mapa,m11["Y"],sqrt(S11['Y','Y'])))
  X2<- pi2*dnorm(mapa,m22["X"],sqrt(S22['X','X']))%*%t(dnorm(mapa,m22["Y"],sqrt(S22['Y','Y'])))
  return(X1/(X1+X2))
}
#wektor podziałki powierzchni XY
podzialka_mapy<-seq(-10,10,0.1)
#mapa p. posteriori
mapa_posteriori<-matrix(Bayes_posteriori_map(podzialka_mapy,m11,m22,S11,S22,pi1,pi2),length(podzialka_mapy))
#porównanie. Niepieska linia z contour pochodząca z obliczeń nakłada się z granicą ustaloną przez drawparti
with(data,drawparti(class,X,Y,method = "naiveBayes",xlab = "X",ylab = "Y"))
contour(podzialka_mapy, podzialka_mapy, mapa_posteriori, add = T, levels = 0.5, lwd = 2, lty = 2, col = "blue")





  




